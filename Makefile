local:
	docker compose --profile=local build
	docker compose --profile=local up -d --remove-orphans
	docker compose --profile=local logs -f

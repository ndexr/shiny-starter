options(
  tibble.print.max = 50,
  tibble.print_min = 5,
  tibble.width = 3000,
  tibble.max_extra_cols = 100,
  pillar.bold = TRUE,
  pillar.subtle = FALSE,
  pillar.neg = TRUE,
  pillar.sigfig = 2,
  pillar.min_title_chars = 15
)

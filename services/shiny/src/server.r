#' @export
server <- function(input, output, session) {
  {
    # box::use(shinymanager)
    box::use(. / r / frontend)
    # box::use(shiny)
  }
  frontend$server_frontend("frontend")
}

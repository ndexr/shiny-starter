#' @export
ui_frontend <- function(id) {
  {
    box::use(shiny = shiny[tags])
    box::use(shinyjs)
    box::use(. / sidebar / sidebar)
    box::use(. / router)
    box::use(. / footer / footer)
    box::use(. / users / subscribe)
    box::use(bs4Dash)
  }

  ns <- shiny$NS(id)
  shiny$addResourcePath(prefix = "javascript", directoryPath = "./javascript")
  shiny$addResourcePath(prefix = "css", directoryPath = "./css")

  tags$html(
    lang = "en",
    tags$head(
      tags$meta(charset = "utf-8"),
      tags$meta(name = "viewport", content = "width=device-width, initial-scale=1, shrink-to-fit=no"),
      tags$title("ndexr"),
      tags$meta(name = "author", content = "Freddy Drennan"),
      tags$meta(name = "description", content = "r apps on aws"),
      tags$link(rel = "icon", type = "image/png", href = "./www/img/ndexrsym.png"),
      shinyjs$useShinyjs(),
      shiny$includeCSS("./css/styles.css", rel = "stylesheet"),
      tags$link(rel = "stylesheet", href = "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css")
    ),
    tags$body(
      id = "page-top",
      tags$nav(
        class = "bg-dark",
        sidebar$ui_sidebar(ns("sidebar"))
      ),
      bs4Dash$bs4DashPage(
        header = bs4Dash$dashboardHeader(),
        sidebar = bs4Dash$dashboardSidebar(disable = TRUE),
        body = bs4Dash$dashboardBody(
          router$app_router(ns)$ui
        ),
        footer = bs4Dash$dashboardFooter()
      ),
      footer$ui_footer(ns("footer")),
      shiny$includeScript("../node_modules/d3/dist/d3.min.js"),
      shiny$includeScript("../node_modules/@popperjs/core/dist/umd/popper.min.js"),
      shiny$includeScript("../node_modules/bootstrap/dist/js/bootstrap.min.js"),
      shiny$includeScript("../node_modules/js-cookie/dist/js.cookie.min.js"),
      shiny$includeScript("./javascript/cookies.js"),
      shiny$includeScript("./javascript/inittooltip.js"),
      shinyjs$extendShinyjs("./javascript/bgRandom.js", functions = "bgRandom")
    )
  )
}

#' @export
server_frontend <- function(id) {
  {
    box::use(shiny = shiny[withTags, tags])
    box::use(. / router)
    box::use(shiny.router)

    box::use(. / sidebar / sidebar)
    box::use(. / login / user_login)
    box::use(. / login / user_create)
    box::use(. / footer / footer)
    box::use(. / users / subscribe)
  }


  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      router$app_router(ns)$server(input, output, session)

      login <- user_login$server_user_login("user_login")

      footer$server_footer("footer")
      subscribe$server_subscribe("home")
      user_create$server_user_create("user_create")
      shiny$observeEvent(
        login(),
        {
          box::use(shiny.router)
          shiny.router$change_page("home")
          sidebar$server_sidebar("sidebar")
        }
      )
    }
  )
}

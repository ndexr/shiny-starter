#' @export
connection_redis <- function() {
  box::use(redux)
  con <- redux$hiredis(host = Sys.getenv("REDIS_HOST"), port = Sys.getenv("REDIS_PORT"))
}

#' @export
setDefault <- function(value, default) {
  if (length(value) > 1) {
    return(value)
  }
  ifelse(is.null(value) | !length(nchar(value)), default, value)
}


#' @export
store_state <- function(id, data) {
  box::use(. / redis)
  box::use(glue[glue])
  box::use(redux)
  box::use(shiny)
  box::use(storr)
  box::use(. / postgres)
  if (isTRUE(getOption("docker"))) {
    con <- redis$connection_redis()
    data <- redux$object_to_bin(data)
    con$SET(key = id, value = data)
  } else {
    st <- storr$storr_dbi(tbl_data = "db", "dbkeys", postgres$connection_postgres())
    st$set(id, data)
  }
}

#' @export
get_state <- function(id) {
  box::use(. / redis)
  box::use(glue[glue])
  box::use(redux)
  box::use(shiny)
  box::use(storr)
  box::use(. / postgres)
  if (!getOption("docker")) {
    st <- storr$storr_dbi(tbl_data = "db", "dbkeys", postgres$connection_postgres())
    if (st$exists(id)) data <- st$get(id) else return(list())
  } else {
    con <- redis$connection_redis()
    data <- con$GET(key = id)
    if (!is.null(data)) data <- redux$bin_to_object(data)
    if (shiny$is.reactivevalues(data)) {
      return(list())
    }
    data
  }
}

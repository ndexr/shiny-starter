#' @export
textInput <- function(id, label = "@", value = "", type = "text") {
  box::use(shiny = shiny[tags])
  tags$div(
    class = "input-group mb-3",
    tags$span(
      class = "input-group-text",
      `for` = id,
      # id = id,
      label
    ),
    tags$input(
      id = id,
      type = type,
      class = "form-control",
      placeholder = value,
      `aria-label` = value,
      `aria-describedby` = id
    )
  )
}



#' @export
card_collapse <- function(ns, title = "Server", footer = NULL, body = NULL, width = 12,
                          sidebar = NULL,
                          collapsible = TRUE, collapsed = TRUE, closable = TRUE,
                          maximizable = TRUE) {
  {
    box::use(shiny = shiny[tags])
    box::use(glue)
    box::use(bs4Dash)
  }
  hash <- function(x) paste0("#", x)


  bs4Dash$bs4Card(
    body,
    id = ns("card"), sidebar = sidebar,
    collapsible = collapsible, collapsed = collapsed, closable = closable,
    maximizable = maximizable,
    title = title, footer = footer, width = width
  )
}

#' @export
input_selectize <- function(id) {
  box::use(shiny = shiny[tags])
  tags$select(
    id = id, `multiple placeholder` = "Pick a tool..."
  )
}

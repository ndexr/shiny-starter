#' @export
ui_user_login <- function(id = "login") {
  box::use(shiny = shiny[tags])
  box::use(shinyjs)
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_user_login <- function(id = "login", logged_in = TRUE) {
  {
    box::use(shiny = shiny[tags])
    box::use(shinyjs)
    box::use(shinyvalidate)
    box::use(glue)
    box::use(jsonlite)
    box::use(DBI)
    box::use(httr)
    box::use(shiny.router)
    box::use(.. / connections / postgres)
    box::use(uuid)
    box::use(shinyjs[js])
    box::use(.. / inputs / inputs)
    box::use(.. / users / users)
    box::use(bcrypt)
    box::use(uuid)
    box::use(shiny.router)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      output$ui <- shiny$renderUI({
        tags$div(
          id = ns(id),
          class = "row",
          tags$div(class = "col-xl-4 col-lg-3 col-sm-3"),
          tags$div(
            class = "col-xl-4 col-lg-6 col-sm-6 well bg-light p-4",
            tags$h4("Log In", class = "text-center"),
            tags$form(
              inputs$textInput(ns("email"), label = "su -"),
              inputs$textInput(ns("password"), label = "passwd", type = "password"),
              tags$div(
                class = "row",
                tags$div(
                  class = "col d-flex justify-content-between",
                  shiny$actionButton(ns("signUp"), "Go To Waitlist", class = "btn btn-secondary btn-sm"),
                  tags$button(
                    id = ns("submit"),
                    type = "submit",
                    class = "btn btn-primary action-button",
                    "Submit"
                  )
                )
              )
            )
          )
        )
      })

      shiny$observeEvent(input$signUp, {
        shiny.router$change_page("user_create")
      })



      app_session_start <- shiny$eventReactive(input$submit, {
        uuid <- uuid$UUIDgenerate()
        email <- input$email
        password <- input$password

        # set cookies
        msg <- list(name = "logintime", value = as.numeric(Sys.time()), id = ns("cookie"))
        session$sendCustomMessage("cookie-set", msg)
        msg <- list(name = "uuid", value = uuid, id = ns("cookie"))
        session$sendCustomMessage("cookie-set", msg)

        app_session_start
      })

      app_session_start
    }
  )
}

#' @export
console_add_user <- function(username, password) {
  box::use(.. / connections / postgres)

  console_users <- data.frame(
    username = username, password = password
  )
  postgres$table_create_or_upsert(console_users, "username")
}

if (FALSE) {
  box::use(. / user_login)
  box::reload(user_login)
  # user_login$console_add_user('fdrennan', 'thirdday1')
}

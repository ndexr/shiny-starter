#' @export
ui_subscribe <- function(id) {
  box::use(shinyjs)

  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)


  make_plan <- function(plan_name = "Basic", price = 28, payment_link, descriptors) {
    {
      box::use(shiny = shiny[tags])
      box::use(glue)
      box::use(purrr)
    }
    tags$div(
      class = "card mb-4 box-shadow",
      tags$div(
        class = "card-header",
        tags$h4(
          class = "my-0 font-weight-normal",
          plan_name
        )
      ),
      tags$h1(
        class = "card-title",
        tags$div(
          class = "d-flex align-items-center justify-content-center",
          tags$div(glue$glue("${price}"), class = "display-5 text-dark"),
          tags$small(
            class = "text-muted",
            "/ mo"
          )
        )
      ),
      tags$div(
        class = "card-body",
        tags$ul(
          class = "mt-3 mb-4",
          purrr$map(
            descriptors, function(x) {
              tags$li(class = "text-left", x)
            }
          )
        )
      ),
      tags$div(
        class = "card-footer",
        tags$a(href = payment_link, target = "_blank", glue$glue("Buy {plan_name} Plan"), class = "btn")
      )
    )
  }


  tags$div(
    class = "row text-dark",
    shinyjs$useShinyjs(),
    # shinyjs$extendShinyjs(text = "shinyjs.goToTop = function() {document.body.scrollTop = 0;}" , functions = 'goToTop'),
    # shinyjs$extendShinyjs(text = "shinyjs.goToTop = function() {window.scrollTo(0, 0);}", functions = 'goToTop'),
    # tags$div(
    # class = "col-md-6 offset-md-3",
    shiny::includeHTML("README.html")
    # )
  )
}

#' @export
server_subscribe <- function(id) {
  {
    box::use(shiny = shiny[tags])
    box::use(shinyjs)
  }
  shiny$moduleServer(
    id, function(input, output, session) {
      shiny$observeEvent(
        input$goToLogin,
        {
          box::use(shiny.router)
          shiny.router$change_page("user_login")
          shinyjs$runjs("window.scrollTo(0, 0)")
        }
      )
    }
  )
}


#' @export
stripe_product_create <- function(name) {
  box::use(reticulate)
}

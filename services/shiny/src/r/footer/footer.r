#' @export
ui_footer <- function(id) {
  {
    box::use(shiny = shiny[tags])
    box::use(shiny.router[route_link])
    box::use(glue)
    # box::use(../gmail/gmail)
  }
  ns <- shiny$NS(id)
  out <- tags$div(
    class = "navbar navbar-dark bg-dark d-flex justify-content-center",
    {
      tags$footer(
        class = "p-3",
        tags$div(
          class = "row",
          tags$div(
            class = "col-12 col-md-6 mb-3",
            tags$h5(
              "Local",
              class = "font-weight-light"
            ),
            tags$div(
              class = "border border-light bg-light mb-3"
            ),
            tags$ul(
              class = "nav flex-column",
              tags$li(
                class = "nav-item p-1",
                tags$a(
                  href = route_link(tolower("home")),
                  class = "hoverable nav-link text-muted",
                  title = "home",
                  `data-bs-toggle` = "tooltip",
                  `data-bs-placement` = "right",
                  `data-bs-original` = "home",
                  tags$div(
                    class = "d-flex justify-content-start align-items-center",
                    tags$h5("Home", class = "me-2"),
                    tags$i(
                      class = "bi bi-house-fill fa"
                    )
                  )
                )
              )
            )
          ),
          tags$div(
            class = "col-md-6 mb-3",
            tags$h5(
              "Remote",
              class = "font-weight-light"
            ),
            tags$div(
              class = "border border-light bg-light mb-3"
            ),
            tags$ul(
              class = "nav flex-column",
              tags$li(
                class = "nav-item p-1",
                tags$a(
                  target = "_blank",
                  href = "https://console.aws.amazon.com/console/home",
                  class = "hoverable nav-link text-muted",
                  title = "cpu",
                  `data-bs-toggle` = "tooltip",
                  `data-bs-placement` = "right",
                  `data-bs-original` = "aws",
                  tags$div(
                    class = "d-flex justify-content-start align-items-center",
                    tags$h5("AWS Console", class = "me-2"),
                    tags$i(
                      class = "bi-cloud-fill fa"
                    )
                  )
                )
              )
            )
          )
        ),
        tags$div(
          class = "d-flex flex-column justify-content-around align-items-center flex-sm-row text-center  py-4 my-4 border-top ",
          tags$p(
            "© 2023 NDEXR. All rights reserved."
          )
        )
      )
    }
  )

  out
}

#' @export
server_footer <- function(id) {
  {
    box::use(glue)
    box::use(shiny = shiny[tags])
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      output$dockerstatus <- shiny$renderUI({
        tags$em(
          glue$glue('docker: {getOption("docker")}')
        )
      })

      shiny$observeEvent(input$dockertoggle, {
        options(docker = !getOption("docker"))
        output$dockerstatus <- shiny$renderUI({
          tags$em(
            glue$glue('docker: {getOption("docker")}')
          )
        })
      })

      shiny$observeEvent(input$subscribe, {
        box::use(.. / connections / postgres)
        newsletter <- data.frame(
          email = input$newsletter,
          info = input$info
        )

        postgres$table_append(newsletter)
        shiny$showNotification("Thanks!")
        box::use(.. / gmail / gmail)
        gmail$send_email(
          from = "fdrennan@ndexr.com",
          to = "fdrennan@ndexr.com",
          subject = input$newsletter,
          body = input$info,
          key = NULL,
          data_list = NULL
        )
      })
    }
  )
}

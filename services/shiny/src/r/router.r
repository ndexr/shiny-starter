#' @export
app_router <- function(ns) {
  box::use(shiny.router)
  box::use(. / login / user_login)
  box::use(. / login / user_create)
  box::use(. / users / subscribe)

  shiny.router$make_router(
    shiny.router$route("home", subscribe$ui_subscribe(ns("home"))),
    shiny.router$route("user_login", user_login$ui_user_login(ns("user_login"))),
    shiny.router$route("user_create", user_create$ui_user_create(ns("user_create")))
    # shiny.router$route("terminal", terminal$ui_terminal(ns("terminal")))
  )
}
